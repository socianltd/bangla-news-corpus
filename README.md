# bangla-news-corpus
A collection of 20,000 news articles categorized according to their type (political, sports, domestic, international, religious, crime etc.). The dataset has a total word count of 2 million (pre-processed). It was collected from popular Bangla news portals (Prothom Alo, Kaler Kantho, Ittefaq etc)  It can be used for training article classification models, clustering etc. The corpus is licensed under MIT License and it's available for download. 


We are continuously adding new data to the corpus as our system collects more data. We encourage more research on Bangla Natural Language Processing by actively contributing.

